<head>
    <?php
        include "head.php";
    ?>
    <title>Ajout d'extension en cours... | My Hearthstone Collection</title>
</head>

<?php
	if(isset($_GET['extension']) && isset($_GET['total']) && isset($_GET['nb_cards']) && isset($_GET['booster']) && isset($_GET['oldext'])){
		$oldext = $_GET['oldext'];
		$extension = $_GET['extension'];
		$total = $_GET['total'];
		$nb_cartes = $_GET['nb_cards'];
		$boosters = $_GET['booster'];

		if(isset($_GET['hidden'])){
			$modif_ext = $bdd->prepare('UPDATE extensions SET EName = ?, ETotalCards = ?, ENbCards = ?, EBoosters = ?, EHidden = 1 WHERE EName = ?');
			$modif_ext->execute(array($extension, $total, $nb_cartes, $boosters, $oldext));
		} else {
			$modif_ext = $bdd->prepare('UPDATE extensions SET EName = ?, ETotalCards = ?, ENbCards = ?, EBoosters = ?, EHidden = 0 WHERE EName = ?');
			$modif_ext->execute(array($extension, $total, $nb_cartes, $boosters, $oldext));
		}

		header("location: extensions.php");
	} else {
		header("location: extensions.php");
	}
?>