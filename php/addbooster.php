<head>
    <?php
        include "head.php";
    ?>
    <title>Ajout de booster en cours... | My Hearthstone Collection</title>
</head>

<?php
	if(isset($_GET['extension']) && isset($_GET['nb_cartes']) && isset($_GET['legendaire'])){
		$extension = $_GET['extension'];
		$nb_cartes = $_GET['nb_cartes'];
		
		if($_GET['legendaire'] == 'oui'){
			$add_leg = $bdd->prepare('UPDATE extensions SET ENbCards = ENbCards + ?, EBoosters = 0 WHERE EName = ?;');
			$add_leg->execute(array($nb_cartes, $extension));
		} else {
			$add_no_leg = $bdd->prepare('UPDATE extensions SET ENbCards = ENbCards + ?, EBoosters = EBoosters + 1 WHERE EName = ?;');
			$add_no_leg->execute(array($nb_cartes, $extension));		
		}
		header("location: accueil.php");
	} else {
		header("location: accueil.php");
	}
?>