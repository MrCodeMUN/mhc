<head>
    <?php
        include "head.php";
    ?>
    <title>Ajout d'extension en cours... | My Hearthstone Collection</title>
</head>

<?php
	if(isset($_GET['nom_extension']) && isset($_GET['total_cartes']) && isset($_GET['nb_cartes']) && isset($_GET['boosters_sans_legendaire'])){
		$extension = $_GET['nom_extension'];
		$total = $_GET['total_cartes'];
		$nb_cartes = $_GET['nb_cartes'];
		$legendaire = $_GET['boosters_sans_legendaire'];

		$check_ext = $bdd->prepare('SELECT * FROM extensions WHERE EName = ?;');
		$check_ext->execute(array($extension));

		$data = $check_ext->fetchAll(PDO::FETCH_ASSOC);

		if(count($data) == 0){
			$add_ext = $bdd->prepare("INSERT INTO `extensions` (`EName`, `ETotalCards`, `ENbCards`, `EBoosters`, `EHidden`) VALUES (?, ?, ?, ?, '0');");
			$add_ext->execute(array($extension, $total, $nb_cartes, $legendaire));
		}

		header("location: extensions.php");
	} else {
		header("location: extensions.php");
	}
?>