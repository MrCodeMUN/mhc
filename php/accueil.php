<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php
            include "head.php";
        ?>
        <title>Accueil | My Hearthstone Collection</title>
    </head>

    <body>
        <div class="page">
            <div class="header">
                <a href="accueil.php"><img src="../images/favicon.png" height="150" width="auto"></a>

                <h1>My Hearthstone Collection</h1>
            </div>

            <h2>Ajouter un nouveau booster</h2>

            <form action="addbooster.php" method="GET">
                <div class="boost">
                    <div class="boost-tab">
                        <table>
                            <tr>
                                <th>Extension du booster</th>
                                <th>Nombre de nouvelles cartes obtenues</th>
                                <th>Légendaire trouvée ?</th>
                            </tr>
                            
                            <tr>
                                <th>
                                    <select name="extension">
                                        <?php
                                            $get_ext_name = $bdd->prepare('SELECT EName FROM extensions;');
                                            $get_ext_name->execute(array());

                                            while($ext=$get_ext_name->fetch()){
                                                echo '<option>
                                                    '.$ext['EName'].'
                                                    </option>';
                                            }
                                        ?>
                                    </select>
                                </th>

                                <th>
                                    <input type="number" name="nb_cartes" min="0" max="5" autocomplete="off" required="true"/>
                                </th>

                                <th>
                                    <select name="legendaire">
                                        <option value="oui">Oui</option>
                                        <option selected value="non">Non</option>
                                    </select>
                                </th>
                            </tr>
                        </table>
                    </div>

                    <div class="boost-bouton">
                        <input type="submit" value="Ajouter un booster" />
                    </div>
                </div>
            </form>

            <h2>Liste des extensions</h2>

            <div class="acc-tab-ext">
                <table>
                    <tr>
                        <th>Nom de l'extension</th>
                        <th>Boosters ouverts d'affilée sans légendaire</th>
                        <th>Nombre de cartes possédées</th>
                        <th>Nombre de cartes du set</th>
                        <th>Nombre de cartes restantes</th>
                        <th>Pourcentage de complétion du set</th>
                    </tr>

                    <?php
                        $get_ext = $bdd->prepare('SELECT * FROM extensions ORDER BY EnbCards ASC;');
                        $get_ext->execute(array());

                        while($ext=$get_ext->fetch()){
                            if($ext['EHidden'] == 0){
                                echo '<tr>
                                <th>'.$ext['EName'].'</th>
                                <th>'.$ext['EBoosters'].'</th>
                                <th>'.$ext['ENbCards'].'</th>
                                <th>'.$ext['ETotalCards'].'</th>
                                <th>'.($ext['ETotalCards']-$ext['ENbCards']).'</th>
                                <th>'.(($ext['ENbCards']/$ext['ETotalCards'])*100).' %</th>
                                </tr>';
                            }
                        }
                    ?>
                </table>
            </div>

            <div class="bouton-aff-ext">
                <form action="extensions.php" method="post"><input type="submit" value="Modifier les extensions"></form>
            </div>
        </div>
    </body>
</html>