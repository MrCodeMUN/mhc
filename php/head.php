<?php
	session_start();
	try{
        $bdd = new PDO('mysql:host=localhost;dbname=mhc', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    }
    catch (Exception $e){ // Si erreur
		die('Erreur : la connexion à la base de données a échoué.');
    }
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="../images/favicon.png" type="image/png" sizes="16x16">