<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php
            include "head.php";
        ?>
        <title>Extensions | My Hearthstone Collection</title>
    </head>

    <body>
        <div class="page">
            <div class="header">
                <a href="accueil.php"><img src="../images/favicon.png" height="150" width="auto"></a>

                <h1>Modifier les extensions</h1>
            </div>

    	<h2>Ajouter une nouvelle extension</h2>
    	
    	<form action="addextension.php">
            <div class="ext">
                <div class="ext-tab">
            		<table>
            			<tr>
            				<th>Nom de l'extension</th>
            				<th>Nombre total de cartes dans l'extension</th>
            				<th>Nombre de cartes dans la collection</th>
                            <th>Boosters ouverts d'affilée sans légendaire</th>
            			</tr>
            			<tr>
            				<th>
            					<input type="text" name="nom_extension" required="true" autocomplete="off"/>
            				</th>
                            <th>
                                <input type="number" name="total_cartes" required="true" min="1" max="1000" autocomplete="off"/>
                            </th>
            				<th>
            					<input type="number" name="nb_cartes" required="true" min="0" max="1000" autocomplete="off"/>
            				</th>
            				<th>
            					<input type="number" name="boosters_sans_legendaire" required="true" min="0" max="41" autocomplete="off"/>
            				</th>
            			</tr>
            		</table>
                </div>
                <div class="ext-bouton">
                    <input type="submit" value="Ajouter une extension" />
                </div>
            </div>
    	</form>

        <h2>Liste des extensions</h2>

    	<table>
    		<tr>
    			<th>Nom de l'extension</th>
    			<th>Boosters ouverts d'affilée sans légendaire</th>
    			<th>Nombre de cartes possédées</th>
    			<th>Nombre de cartes du set</th>
    			<th>Nombre de cartes restantes</th>
    			<th>Pourcentage de complétion du set</th>
                <th>Cacher l'extension ?</th>
                <th>Modifier l'extension</th>
    		</tr>
    		
            <?php
                $get_ext = $bdd->prepare('SELECT * FROM extensions;');
                $get_ext->execute(array());

                while($ext=$get_ext->fetch()){
                    echo '<tr><form action="modifyext.php" method="GET">
                    <input type="hidden" name="oldext" value="'.$ext['EName'].'">
                    <th><input type="text" name="extension" value="'.$ext['EName'].'" required></th>
                    <th><input type="number" name="booster" value="'.$ext['EBoosters'].'" min="0" max="41" required></th>
                    <th><input type="number" name="nb_cards" value="'.$ext['ENbCards'].'" min="0" max="1000" required></th>
                    <th><input type="number" name="total" value="'.$ext['ETotalCards'].'" min="1" max="1000" required></th>
                    <th>'.($ext['ETotalCards']-$ext['ENbCards']).'</th>
                    <th>'.(($ext['ENbCards']/$ext['ETotalCards'])*100).' %</th>';
                    
                    if($ext['EHidden'] == 0){
                        echo '<th><input type="checkbox" name="hidden"></th>
                        <th><input type="submit" value="Modifier"></th>
                        </form>
                        </tr>';
                    } else {
                        echo '<th><input type="checkbox" name="hidden" checked></th>
                        <th><input type="submit" value="Modifier"></th>
                        </form>
                        </tr>';
                    }
                }
            ?>
       	</table>

        <div class="bouton-acc">
            <form action="accueil.php" method="post"><input type="submit" value="Revenir à l'accueil"></form>
        </div>
        </div>
    </body>
</html>
